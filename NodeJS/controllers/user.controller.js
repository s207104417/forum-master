const mongoose = require('mongoose');
const passport = require('passport');
const _ = require('lodash');

const User = mongoose.model('User');

module.exports.register = (req, res, next) => {
    var user = new User();
    user.userName = req.body.userName;
    user.email = req.body.email;
    user.password = req.body.password;
    user.todo = req.body.todo;
    user.save((err, doc) => {
        if (!err)
            res.send(doc);
        else {
            if (err.code == 11000)
                res.status(422).send(['Duplicate username/email adrress found.']);
            else
                return next(err.code);
        }

    });
}

module.exports.authenticate = (req, res, next) => {
    // call for passport authentication
    passport.authenticate('local', (err, user, info) => {       
        // error from passport middleware
        if (err) return res.status(400).json(err);
        // registered user
        else if (user) return res.status(200).json({ "token": user.generateJwt() });
        // unknown user or wrong password
        else return res.status(404).json(info);
    })(req, res);
}

module.exports.userProfile = (req, res, next) =>{
    User.findOne({ _id: req._id },
        (err, user) => {
            if (!user)
                return res.status(404).json({ status: false, message: 'User record not found.' });
            else
                return res.status(200).json({ status: true, user : _.pick(user,['_id', 'userName','email', 'todo']) });
        }
    );
}

// module.exports.updateTODO = (req, res) => {
//     if (!req.body) {return res.status(400).send({message: "No record with given id :"});}
//     var work = {
//         // _id: req.body._id,
//         todo: req.body.todo,
//     };
//     User.findOneAndUpdate(req.body._id, { $set: work }, { new: true }, (err, doc) => {
//         if (!err) { 
//             res.send(doc); 
//         }
//         else { console.log('Error in TODO Update :' + JSON.stringify(err, undefined, 2)); }
//     });
// };
module.exports.updateTODO = (req, res) => {
    if (!req.body) {return res.status(400).send({message: "No record with given id :"});}
    var work = {
        // _id: req.body._id,
        todo: req.body.todo,
    };
    User.findByIdAndUpdate(req.params.id, { $set: work }, { new: true }, (err, doc) => {
        if (!err) { 
            res.send(doc); 
        }
        else { console.log('Error in TODO Update :' + JSON.stringify(err, undefined, 2)); }
    });
};