const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { post } = require('../models/post');

// => localhost:3000/posts/
//get all post
router.get('/', (req, res) => {
    post.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving posts :' + JSON.stringify(err, undefined, 2)); }
    });
});

//get post by id
router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

        post.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving post :' + JSON.stringify(err, undefined, 2)); }
    });
});

//add post 
router.post('/', (req, res) => {
    var po = new post({
        userName: req.body.userName,
        title: req.body.title,
        detail: req.body.detail,
    });
    po.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in post Save :' + JSON.stringify(err, undefined, 2)); }
    });
});

//update post
router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var po = {
        userName: req.body.userName,
        title: req.body.title,
        detail: req.body.detail,
    };
    post.findByIdAndUpdate(req.params.id, { $set: po }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in post Update :' + JSON.stringify(err, undefined, 2)); }
    });
});

//delete post
router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

        post.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in post Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});

//delete all
router.delete('/', (req, res) => {
    post.deleteMany((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in post Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});

// router.get('/', (req, res) => {
//     post.find((err, docs) => {
//         if (!err) { res.send(docs); }
//         else { console.log('Error in Retriving posts :' + JSON.stringify(err, undefined, 2)); }
//     });
// });

module.exports = router;