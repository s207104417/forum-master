const mongoose = require('mongoose');

var post = mongoose.model('post', {
    userName: { type: String },
    title: { type: String },
    detail: { type: String }
});

module.exports = { post };