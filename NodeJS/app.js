require('./config/config');
require('./models/db');
require('./config/passportConfig');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

const rtsIndex = require('./routes/index.router');

// const { mongoose } = require('./db.js');
var postController = require('./controllers/postController.js');
var userController = require('./controllers/user.Controller.js');

var app = express();
app.use(bodyParser.json());
app.use(cors({ origin: 'http://localhost:4200' }));
app.use(passport.initialize());
app.use('/api', rtsIndex);

app.get('/', (req, res) => {
    res.send("This is home");
});

app.use((err, req, res, next) => {
    if (err.name === 'ValidationError') {
        var valErrors = [];
        Object.keys(err.errors).forEach(key => valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors)
    }
    else{
        console.log(err);
    }
});

app.use('/posts', postController);
// app.user('/api', userController);

app.listen(process.env.PORT, () => console.log(`Server started at port : ${process.env.PORT}`));