import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Post } from './post.model';

@Injectable()
export class PostService {
  selectedPost: Post;
  posts: Post[];
  readonly baseURL = 'http://localhost:3000/posts'

  constructor(private http: HttpClient) { }

  //post data
  postPost(po: Post) {
    return this.http.post(this.baseURL, po);
  }

  //get posts
  getPostList() {
    return this.http.get(this.baseURL);
  }

  //update post
  putPost(po: Post) {
    return this.http.put(this.baseURL  + `/${po._id}`, po);
  }

  //delete post
  deletePost(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }

  //delete ALL
  deleteALL() {
    return this.http.delete(this.baseURL);
  }
}
