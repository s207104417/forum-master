export class Post {
    _id: string;
    userName: string;
    title: string;
    detail: string;
}
