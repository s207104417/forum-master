import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  selectedUser: User = {
    _id: '',
    userName: '',
    email: '',
    password: '',
    todo: ''
  };
  user: User;
  readonly baseURL = 'http://localhost:3000/api'

  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

  constructor(private http: HttpClient) { }

  //HttpMethods
  //register account
  postUser(user: User){
    return this.http.post(this.baseURL+'/register',user,this.noAuthHeader);
  }
  
  //login account
  login(authCredentials) {
    return this.http.post(this.baseURL + '/authenticate', authCredentials,this.noAuthHeader);
  }

  //get user profile
  getUserProfile() {
    return this.http.get(this.baseURL + '/userProfile');
  }

  //update TODO by userID
  putTODO(user: User) {
    console.log(user._id)
    return this.http.put(this.baseURL + '/updateTODO' + `/${user._id}`, user);
  }

  //Helper Methods
  //login set token
  setToken(token: string) {
    localStorage.setItem('token', token);
  }
  //login get token
  getToken() {
    return localStorage.getItem('token');
  }
  //logout delete token
  deleteToken() {
    localStorage.removeItem('token');
  }

  getUserPayload() {
    var token = this.getToken();
    if (token) {
      var userPayload = atob(token.split('.')[1]);
      return JSON.parse(userPayload);
    }
    else
      return null;
  }

  isLoggedIn() {
    var userPayload = this.getUserPayload();
    if (userPayload)
      return userPayload.exp > Date.now() / 1000;
    else
      return false;
  }
}
