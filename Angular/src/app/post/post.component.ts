import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Post } from '../shared/post.model';
import { Router } from "@angular/router";

import { PostService } from '../shared/post.service';
import { UserService } from '../shared/user.service';
import { User } from '../shared/user.model';

declare var M: any;

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  providers: [PostService]
})
export class PostComponent implements OnInit {
  userDetails;

  constructor(private postService: PostService, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.getUserProfile();
    this.resetForm();
    this.refreshPostList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.postService.selectedPost = {
      _id: "",
      userName: "",
      title: "",
      detail: ""
    }
  }

  onSubmit(form: NgForm) {
    console.log(form.value)
    if (form.value.todo == null){
      form.value.todo = "";
    }
    if (form.value._id == null){
      form.value._id = "";
    }
    form.value.userName = this.userDetails.userName;
    if(form.value._id == "") {
      this.postService.postPost(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshPostList();
        M.toast({ html: 'Saved successfully', classes: 'rounded' });
      });
    } else {
      this.postService.putPost(form.value).subscribe((res) => {
        this.resetForm(form);
        this.refreshPostList();
        M.toast({ html: 'Updated successfully', classes: 'rounded' });
      });
    }
  }

  refreshPostList() {
    this.getPostList();
    this.getUserProfile();
  }

  onEdit(po: Post) {
    //selected
    this.postService.selectedPost = po;
  }

  onDel(_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this record?') == true) {
      this.postService.deletePost(_id).subscribe((res) => {
        this.refreshPostList();
        this.resetForm(form);
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

  getPostList() {
    this.postService.getPostList().subscribe((res) => {
      this.postService.posts = res as Post[];
    });
  
  }
  getUserProfile() {
    this.userService.getUserProfile().subscribe(
      res => {
        this.userDetails = res['user'];
        this.userService.user = res as User;
        // console.log(this.userDetails.todo)
      },
      err => { 
        console.log(err);
      }
    );
  }

  onLogout(){
    this.userService.deleteToken();
    this.router.navigate(['/login']);
  }

  updateTODO(form?: NgForm){
    this.userDetails.todo = form.value.todo;
    console.log(this.userDetails)
    this.userService.putTODO(this.userDetails).subscribe((res) => {
      this.refreshPostList();
      this.resetForm(form);
      M.toast({ html: 'Updated successfully', classes: 'rounded' });
    });
  }

  delALL() {
    if (confirm('Are you sure to delete ALL records?') == true) {
      this.postService.deleteALL().subscribe((res) => {
        this.refreshPostList();
        M.toast({ html: 'Deleted successfully', classes: 'rounded' });
      });
    }
  }

}
